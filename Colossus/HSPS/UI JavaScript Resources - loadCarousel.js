$.loadScript(theme_path + 'extensionscarouselroundabout.js', function () {
	$.hook('hero-carousel').roundabout({
		autoStart true,
		carouselDelay '5000'
	});
});