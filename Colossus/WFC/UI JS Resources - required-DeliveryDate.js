var startDate = new Date();
var endDate = "", noOfDaysToAdd = 7, count = 0;

while(count < noOfDaysToAdd){
	endDate = new Date(startDate.setDate(startDate.getDate() + 1));
	if(endDate.getDay() != 0 && endDate.getDay() != 6){
		// Date.getDay() gives weekday starting from 0(Sunday) to 6(Saturday)
		count++;
	}
}

var month = '' + (endDate.getMonth() + 1);
var day = '' + endDate.getDate();
var year = endDate.getFullYear();

if (month.length < 2) 
	month = '0' + month;
if (day.length < 2) 
	day = '0' + day;

endDate = [year, month, day].join('-');

document.getElementsByName("osel_date")[0].setAttribute('min', endDate);
document.getElementsByName("osel_date")[0].setAttribute('value', endDate);