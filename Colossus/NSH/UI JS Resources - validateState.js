// ---- Throw and error and stop form submission when State selection code is unselected  ---- //
function validateState() {
    var stateList = document.getElementById("ShipStateSelect");
    var selectedState = stateList.options[stateList.selectedIndex].value;

    if (selectedState == "unselected") {
        alert("Please select a state.");
        return false;
    }
}