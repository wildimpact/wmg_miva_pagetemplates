$.loadScript(theme_path + 'extensions/carousel/roundabout.js',
function () {
	$.hook('hero-carousel').roundabout({
		autoStart: true,
		carouselDelay: '8000'
	});
});