	// ---- Quantity Incrementer ---- //
	/*
		We had to to build out own quantity incrementer functions to work with the ForceMultiples
		module. Since we have not yet been successfully able to remove Miva's built-in incrementer
		functions, we had to build these to also account for the native functions. That's why you'll
		see the extrea +1 and -1 in the formulas below.
		- Mike M 2020.05.19
	*/
	$('#js-increase-quantity, .js-increase-quantity').on('click', function () {
		var $incr = parseInt($('#js-quantityincr').val())-1;

		var $qty = $(this).parent().prev(),
			currentVal = parseInt($qty.val()),
			$update = $(this).parent().siblings('.js-update-quantity');
		if (!isNaN(currentVal)) {
			$qty.val(currentVal + $incr).change();
			$update.trigger('click');
		}
	});

	$('#js-decrease-quantity, .js-decrease-quantity').on('click', function () {
		var $incr = parseInt($('#js-quantityincr').val());

		var $qty = $(this).parent().prev(),
			currentVal = parseInt($qty.val()),
			min = (!isNaN($qty.data('min'))) ? $qty.data('min') : 1,
			$update = $(this).parent().siblings('.js-update-quantity');

		if (!isNaN(currentVal) && currentVal > $incr) {
			//this is higher than the smallest increment, so just decrement it
			$qty.val(currentVal - $incr + 1).change();
			$update.trigger('click');
		}
		else
		{
			//this will create a result less than our increment, so stop the quantity at our increment
			$qty.val($incr+1).change();
			$update.trigger('click');
		}
	});