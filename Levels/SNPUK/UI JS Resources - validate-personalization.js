// ---- Set the personalization lines to allow no more than 30 characters---- //
var inputPersonalizationLine1 = document.getElementsByName("Product_Attributes[1]:value")[0];
var inputPersonalizationLine2 = document.getElementsByName("Product_Attributes[2]:value")[0];
var inputPersonalizationLine3 = document.getElementsByName("Product_Attributes[3]:value")[0];
var inputPersonalizationLine4 = document.getElementsByName("Product_Attributes[4]:value")[0];
var inputPersonalizationLine5 = document.getElementsByName("Product_Attributes[5]:value")[0];		

if (typeof(inputPersonalizationLine1) != 'undefined' && inputPersonalizationLine1 != null) {
	inputPersonalizationLine1.setAttribute("maxlength", "30");
}

if (typeof(inputPersonalizationLine2) != 'undefined' && inputPersonalizationLine2 != null) {
	inputPersonalizationLine2.setAttribute("maxlength", "30");
}

if (typeof(inputPersonalizationLine3) != 'undefined' && inputPersonalizationLine3 != null) {
	inputPersonalizationLine3.setAttribute("maxlength", "30");
}

if (typeof(inputPersonalizationLine4) != 'undefined' && inputPersonalizationLine4 != null) {
	inputPersonalizationLine4.setAttribute("maxlength", "30");
}

if (typeof(inputPersonalizationLine5) != 'undefined' && inputPersonalizationLine5 != null) {
	inputPersonalizationLine5.setAttribute("maxlength", "30");
}