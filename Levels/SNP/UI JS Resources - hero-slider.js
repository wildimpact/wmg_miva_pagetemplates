$.ajax({
	cache: true,
    crossDomain: true,
    dataType: 'script',
    url: theme_path + '/js/jquery.slick.min.js'
}).done(function () {
	// ---- Hero Slide Show ---- //
    $('#js-hero-slider').slick({
    	arrows: true,
        autoplay: true,
        autoplaySpeed: 10000,
        dots: true,
        slide: '',
        responsive: [{
        	breakpoint: 960,
            settings: {
            	dots: false
            }
       	}]
    });
});