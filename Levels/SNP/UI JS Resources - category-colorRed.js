var clearanceLinks = document.querySelectorAll("a[href='https://stores.wildmarketinggroup.com/clearance-9932.html']");
var featuredLinks = document.querySelectorAll("a[href='https://stores.wildmarketinggroup.com/featured.html']");

for (var i = 0, l = clearanceLinks.length; i < l; i++) {
  var clearanceLink = clearanceLinks[i];
  clearanceLink.setAttribute("style", "color: #CC0033;");
}

for (var i = 0, l = featuredLinks.length; i < l; i++) {
  var featuredLink = featuredLinks[i];
  featuredLink.setAttribute("style", "color: #CC0033;");
}