// ---- When GROUND INCLUDED is not selected, show UPS account number verbiage and required input  ---- //
function requireUPSAccountNumber() {
	var radioGroundIncluded = document.getElementById("l-shipping-method-Free Shipping");
	var textExpediteViaUPS = document.getElementById("UPS-only-for-expediting");
	var formUPSAccountNumber = document.getElementById("UPSAccountNumberForm");
	var numberUPSAccountNumber = document.getElementById("l-osel_UPSAccountNumber");

	if ($(radioGroundIncluded).is(":checked")) {
		textExpediteViaUPS.style.display = "none";
		formUPSAccountNumber.style.display = "none";
		numberUPSAccountNumber.required = false;
		} else {
		textExpediteViaUPS.style.display = "inline-block";
		formUPSAccountNumber.style.display = "inline-block";
		numberUPSAccountNumber.required = true;
	}
}