// ---- Hide ship partial when verbiage Ship Complete is checked ---- //
function hideShipPartialVerbiage() {
	var inputShipComplete = document.getElementsByName("osel_shipComplete");
	var shipPartialVerbiage = document.getElementById("partial-shipping-verbiage");

	if ($(inputShipComplete).is(":checked")) {
		shipPartialVerbiage.style.display = "none";
		} else {
		shipPartialVerbiage.style.display = "inline-block";
	}
}