# README #

wmg_miva_pagetemplates

This repository is for the page templates and personalized Order Confirmations used for each web store within the Miva platform. Order Confirmations are sent to the customer (individual ordering) and merchant (WMG client service team). See our Confluence article for a more in-depth write-up @ https://wildimpact.atlassian.net/wiki/spaces/451/pages/97976359/Email+Templates.

Structure: Miva ReadyTheme > Web Store Code > Template Files

Repo admin: Michael Sherman / 414-908-3953 / msherman@wildmarketinggroup.com

Server mgr: Mike McGraw

Deployment: Michael Sherman / Sean McCarthy

Confluence space: none yet

## Getting Started ##

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites ###

1. Text editor
2. Git client

### Related Repos ###

1. wmg_miva_emailtemplates : repo for the email templates per customer
2. wmg_visualstudio_themes : repo for the httpdocs directory on our Miva Server
3. wmg_visualstudio_mivalibrary : repo for Miva's API

## Contribution Guidelines ##

* Please use the same variable naming convention for consistency.
* Follow general programming best practices.
* Do not check in changes that do not compile.
* Do not leave changes checked out too long and leave us susceptible to machine failures.
* Every developer is responsible for unit testing his/her own code!
* All commits must have robust comments documenting what changed & why for easier review and troubleshooting down the road.
* Code changes or new updates should be followed up by a pull request to repo admin.
* Make sure content is organized by ReadyTheme > Web Store Code.

## Deployment Instructions ##

When prepared for deployment, create a pull request to the repo admin. The repo admin will access the AWS server and git pull the changes and update the files.